const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const routes = require('./routes');

const app = express();

mongoose.connect('mongodb+srv://omnistack:omnistack@cluster0-xmlq6.mongodb.net/semana09?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// req.query = acessar query params (para filtros) ex: ?idade=20
// req.params = acessar route params (para edição e delete) ex: /1
// req.body = acessar corpo da requisição (para criação e edição) (geralmente em json)

//para o express entender que vai utilizar formato json
app.use(express.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(routes);

app.listen(3333);